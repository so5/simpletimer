#
# Compiler Setting
#
include Mkinclude/Mkinclude.GNU_OpenMPI
#include Mkinclude.Intel_IntelMPI


LIB=lib/libSimpleTimer.a
SRCS_f90  = src/SimpleTimer_interface.f90
SRCS_cpp  = src/SimpleTimer.cpp
OBJS_F90=$(SRCS_f90:%.f90=%.o)
OBJS_CXX=$(SRCS_cpp:%.cpp=%.o)
OBJS=$(OBJS_F90) $(OBJS_CXX)

#
# Additional compiler/linker flags
#
FFLAGS   += -g
CFLAGS   += -g -Iinclude
CXXFLAGS += -g -Iinclude


#
# actual build command
#
all:$(LIB) f_sample cpp_sample c_sample f_sample2

$(LIB):$(OBJS)
	-mkdir lib
	ar rv $(LIB) $^

f_sample: sample/f_sample.o $(LIB) 
	-mkdir bin
	$(FC) $(FFLAGS) $(LDFLAGS) $^ -o bin/$@

f_sample2: sample/f_sample2.o $(LIB) 
	-mkdir bin
	$(FC) $(FFLAGS) $(LDFLAGS) $^ -o bin/$@

c_sample: sample/c_sample.o $(LIB) 
	-mkdir bin
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o bin/$@

cpp_sample: sample/cpp_sample.o $(LIB) 
	-mkdir bin
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $^ -o bin/$@

clean:
	-rm -rf src/*.o $(LIB) sample/*.o

.PHONY: all clean

#
# suffix rules
#
.SUFFIXES:.f90 .F90
.f90.o:
	$(FC) $(FFLAGS) -o $@ -c $<
.F90.o:
	$(FC) $(FFLAGS) -o $@ -c $<

