#include <mpi.h>
#include <unistd.h>
#include <iostream>
#include <string>
#include <map>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <omp.h>

#include "SimpleTimer.h"

extern "C"{
void simple_timer_begin__(const char* label);
void simple_timer_end__(const char* label);
}

namespace SimpleTimer
{
    struct Time
    {
        double start;
        double accumulated;
        long   num_call;
        bool   measuring;
        bool   illegal;
        Time():measuring(false), illegal(false), num_call(0){}
    };

    class DataBase
    {
        DataBase()
        {
            int nproc;
            int myrank;
            MPI_Comm_size(MPI_COMM_WORLD, &nproc);
            MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
            std::stringstream ss;
            ss<<"SimpleTimer_"<<std::setfill('0') << std::setw(std::log10(nproc)+1) <<myrank<<".csv";
            filename=ss.str();
            std::ofstream out(filename.c_str());
            out<<"Simple Timer Output"<<std::endl;
            char hostname[80];
            gethostname(hostname, 80);
            out<<"  Hostname    = "<<hostname<<std::endl;
            out<<"  Rank ID     = "<<myrank<<std::endl;
            out<<"  NProc       = "<<nproc<<std::endl;
            out<<"  MPI_Wtick() = "<<MPI_Wtick()<<std::endl;
            out<<std::endl;
        }

        ~DataBase()
        {
            std::ofstream out(filename.c_str(), std::ofstream::app);
            out<<"total number of measured sections = "<<Table.size()<<std::endl;
            out<<std::endl;
            out<<"name, elapsed, num_call"<<std::endl;
            for(std::map<std::string, Time>::iterator it = Table.begin(); it!= Table.end(); ++it)
            {
                if ((*it).second.illegal)
                {
                  out <<"*";
                }
                out << (*it).first<<","<<(*it).second.accumulated<<","<<(*it).second.num_call<<std::endl;
            }
            out<<std::endl;
        }

        DataBase(const DataBase& obj);

        DataBase& operator=(const DataBase& obj);

        public:
        void begin(const std::string& arg_label)
        {
            std::string label;
            if(omp_in_parallel())
            {
                std::stringstream ss;
                ss << omp_get_thread_num();
                label=arg_label+'_'+ss.str();
            }else{
                label=arg_label;
            }
            std::map<std::string, Time>::iterator it = Table.find(label);
            if (it != Table.end())
            {
               if((*it).second.measuring == true)
               {
                (*it).second.illegal=true;
               }
               if((*it).second.illegal)
               {
                 std::cerr <<"WARN: "<<label<<" is marked as illegal"<<std::endl;
               }
            }else{
                Time tmp;
                #pragma omp critical
                {
                    Table[label]=tmp;
                }
                it = Table.find(label); 
            }
            ++((*it).second.num_call);
            (*it).second.measuring=true;
            (*it).second.start=MPI_Wtime();
        }

        void end(const std::string& arg_label)
        {
            double now=MPI_Wtime();
            std::string label;
            if(omp_in_parallel())
            {
                std::stringstream ss;
                ss << omp_get_thread_num();
                label=arg_label+'_'+ss.str();
            }else{
                label=arg_label;
            }
            std::map<std::string, Time>::iterator it = Table.find(label);
            if (it == Table.end())
            {
              std::cerr <<"WARN: "<<label<<" not found"<<std::endl;
              return;
            }
            if (!(*it).second.measuring)
            {
              std::cerr <<"WARN: "<<label<<" is not active region"<<std::endl;
              return;
            }
            (*it).second.measuring=false;
            (*it).second.accumulated+=now-(*it).second.start;
        }

        static DataBase& GetInstance()
        {
            static DataBase instance;
            return instance;
        }

        private:
        std::string filename;
        std::map<const std::string, Time> Table;
    }; 


    void begin(const std::string label)
    {
        DataBase::GetInstance().begin(label);
    }

    void end(const std::string label)
    {
        DataBase::GetInstance().end(label);
    }
}

//
// interface routine for C
//
void simple_timer_begin(const char* label)
{
    SimpleTimer::begin(label);
}

void simple_timer_end(const char* label)
{
    SimpleTimer::end(label);
}

//
// interface routine for Fortran
//
void simple_timer_begin__(const char* label)
{
    SimpleTimer::begin(label);
}

void simple_timer_end__(const char* label)
{
    SimpleTimer::end(label);
}
