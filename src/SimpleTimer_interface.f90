subroutine simple_timer_begin(label)
    character(len=*), intent(in) :: label
    character(len=1), parameter  :: null = char(0)
    call simple_timer_begin_(label//null)
end subroutine


subroutine simple_timer_end(label)
    character(len=*), intent(in) :: label
    character(len=1), parameter  :: null = char(0)
    call simple_timer_end_(label//null)
end subroutine
