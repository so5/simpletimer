#ifndef SIMPLE_TIMER_H
#define SIMPLE_TIMER_H
#ifdef __cplusplus
#include <string>
namespace SimpleTimer
{
    void begin(const std::string label);
    void end(const std::string label);
}
extern "C"{
#endif
void simple_timer_begin(const char* label);
void simple_timer_end(const char* label);
#ifdef __cplusplus
}
#endif
#endif
