#include <mpi.h>
#include "SimpleTimer.h"

int main(int argc,char *argv[]){
    double a[10000];
    double b[10000];
    MPI_Init(&argc, &argv);

    simple_timer_begin("first");
        for(int i=0;i<1000;i++)
        {
            a[i]=b[i];
        }
    simple_timer_end("first");

    for(int i=0;i<1000;i++)
    {
        simple_timer_begin("second");
        for(int j=0;j<1000;j++)
        {
            a[j]=b[j];
        }
        simple_timer_end("second");
    }


        MPI_Finalize();

    return 0;
}
