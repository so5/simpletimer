    program sample_code_for_simple_timer
    use mpi
    implicit none
    integer :: i, ierr
    real(8) :: a(1000), b(1000)
    interface
        subroutine simple_timer_begin(label)
            character :: label
        end subroutine simple_timer_begin
        subroutine simple_timer_end(label)
            character :: label
        end subroutine simple_timer_end
    end interface

    call MPI_Init(ierr)
    call simple_timer_begin('first')
    do i=1, 1000
       a=b
    enddo
    call simple_timer_end('first')
    do i=1, 1000
        call simple_timer_begin('second')
           a=b
        call simple_timer_end('second')
    enddo
    call MPI_Finalize(ierr)
    end program
