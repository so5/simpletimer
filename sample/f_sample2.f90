    program sample_code_for_simple_timer
    use mpi
    implicit none
    integer :: i, j, ite, ierr
    integer :: nproc, myrank
    integer, parameter :: n=1024
    real(8),allocatable :: a(:,:)
    interface
        subroutine simple_timer_begin(label)
            character :: label
        end subroutine simple_timer_begin
        subroutine simple_timer_end(label)
            character :: label
        end subroutine simple_timer_end
    end interface


    call MPI_Init(ierr)
    call MPI_Comm_size(MPI_COMM_WORLD, nproc, ierr)
    call MPI_Comm_rank(MPI_COMM_WORLD, myrank, ierr)

    myrank=myrank+1
    allocate(a(n*myrank,n))

    !$omp parallel
    do ite=1, 10
        call simple_timer_begin('first')
        !$omp do schedule(static,256)
        do i=1, n
           do j=1, i
               a(i,j)=0
           enddo
        enddo
        !$omp enddo nowait
        call simple_timer_end('first')


        call simple_timer_begin('second')
        !$omp do
        do i=1, n
           do j=1, n
               a(i,j)=1.0
           enddo
        enddo
        !$omp enddo nowait
        call simple_timer_end('second')

    enddo
    !$omp end parallel

    do ite=1, 10
        call simple_timer_begin('third')
        do i=1, n
           do j=1, n
               a(i,j)=1.0
           enddo
        enddo
        call simple_timer_end('third')
    enddo

    deallocate(a)

    call MPI_Finalize(ierr)
    end program
